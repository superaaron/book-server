# 小说接口服务

1、实现基本四大接口（搜索，获取书籍所有来源，查看书籍，查看章节）

2、实现搜索聚合去重

3、实现公共化配置config.json，增加来源站，只需写正则，方便新手

4、实现图片自动代理

后续持续增加更多来源网站和功能

接口返回成功：

```
{code: 1,data:[],msg: "操作成功"}
```
接口返回失败

```
{code: 0,data:[],msg: "操作失败"}
```

2023-03-30

1、修复书籍查找来源接口。

```
"method": "post",
"post": {
    "参数名": [keyword]
}
```
2、多网站聚合优化，如果A网站没有图片，则用B网站的

config.json 配置文件说明，目前如下，后续可能因不同采集站，进行扩展

```
{
    "bqs": {
        "name": "笔趣说",  // 网站名称
        "host": "https://www.xsxs520.cc/", // 网站地址
        "charset": "gbk",  // 网站编码，如：utf-8，gbk等
        "search": {  // 搜索模块
            "method": "get",  // 搜索请求方式
            "host": "https://www.xsxs520.cc/hxdiugvhsrdighidsghi.php?ie=gbk&q=[keyword]", // 搜索请求地址
            "post": { // 如果是post请求，自定义参数名，[keyword]为接口搜索传输值
                "参数名":"[keyword]"
            },
            "regExp": "<div class=\"bookbox\"><div class=\"p10\"><div class=\"bookimg\"><a href=\"([^\"]*?)\/\"><img src=\"([^\"]*?)\"><\/a><\/div><div class=\"bookinfo\"><h4 class=\"bookname\"><a href=\"([^\"]*?)\">([^\"]*?)<\/a><\/h4><div class=\"cat\">分类：([^\"]*?)<\/div><div class=\"author\">作者：([^\"]*?)<\/div><div class=\"update\"><span>最新章节：<\/span><a href=\"([^\"]*?)\">([^\"]*?)<\/a><\/div><p>([^\"]*?)<\/p><\/div><\/div><\/div>",  // 搜索结果匹配正则
            "img": 2, // 搜索结果正则匹配 从1开始，取第2项 <img src= 后面就是第二项
            "desc": 9, // 搜索结果正则匹配 从1开始，取第9项
            "name": 4,
            "type": 5,
            "author": 6,
            "url": 1,
            "new": 8,
            "newurl": 7
        },
        "book": {// 书籍页正则
            "name": {
                "regExp": "<meta property=\"og:title\" content=\"([^\"]*?)\"\/>",// 书名正则
                "value": 1// 取第1项
            },
            "desc": {
                "regExp": "<meta property=\"og:description\" content=\"([^\"]*?)\"\/>",// 简介正则
                "value": 1
            },
            "type": {
                "regExp": "<meta property=\"og:novel:category\" content=\"([^\"]*?)\"\/>",// 类型正则
                "value": 1
            },
            "author": {
                "regExp": "<meta property=\"og:novel:author\" content=\"([^\"]*?)\"\/>",// 作者正则
                "value": 1
            },
            "img": {
                "regExp": "<meta property=\"og:image\" content=\"http:\/\/www.xsxs520.cc([^\"]*?)\"\/>",// 图片正则
                "value": 1
            },
            "list": {
                "regExp": "<dd><a href =\"([^\"]*?)\">([^\"]*?)<\/a><\/dd>",// 目录正则
                "name": 2,// 取第2项
                "url": 1 // 取第1项
            }
        },
        "read": {// 章节正则
            "name": {
                "regExp": "<h1>([^\"]*?)<\/h1>",// 章节名称正则
                "value": 1
            },
            "text": {
                "regExp": "<div id=\"content\" class=\"showtxt\">([^\"]*?)<br \/><br \/><\/div>", // 章节内容正则
                "value": 1
            },
            "next": {
                "regExp": "<li><a href=\"([^\"]*?)\">下一章<\/a><\/li>",// 下一章节地址正则
                "value": 1
            },
            "up": {
                "regExp": "<li><a href=\"([^\"]*?)\">上一章<\/a><\/li>", // 上一章节地址正则
                "value": 1
            }
        }
    }
}
```

接口文档地址：[https://api.book.bbdaxia.com](https://api.book.bbdaxia.com)